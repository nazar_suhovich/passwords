package com.cray.software.passwords.interfaces;

public interface SyncListener {
    public void endExecution(boolean result);
}
